#### Class 10 | Week 15 | 9 Apr 2019: Algorithms
##### With Wed tutorial session and No Fri shutup and code session (set up your own study group)

### Messy notes:

#### Agenda:

1. Algorithms: What is that and project showcase
2. Peer-tutoring: Group 8 / Respondents: Group 9, Topic: A sorting algorithm
3. in-class ex: draw a flow chart
4. Walkthrough next mini-ex10: Flowchart and next
---

#### 1. Algorithms: What is that and project showcase

**Why algorithms?**

![image2](https://www.webopedia.com/imagesvr_ce/5326/algorithm.gif)

![image4](https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Diagram_for_the_computation_of_Bernoulli_numbers.jpg/1600px-Diagram_for_the_computation_of_Bernoulli_numbers.jpg)

Bernoulli number ‘algorithm’, Ada Lovelace, 1843 (often called the world’s first computer program), [details here](http://www.fourmilab.ch/babbage/sketch.html)
=> Unfold a list of operations/steps to solve this: B7 = −1(A0+B1A1+B3A3+B5A5)

"I want to put something about Bernoulli’s numbers, in one of my Notes, as an example of how the implicit function may be worked out by the engine without human head & hands first. Give me the necessary formulae." (Lovelace Papers, Bodleian Library, Oxford University, 42, folio 12 (6 Feb 1841). As quoted and cited in Dorothy Stein (ed.), 'This First Child of Mine', Ada: A Life and a Legacy (1985), 106-107.)

**Why Flow Chart?**

“[A flow chart] shows the constraints on the system, its system boundaries and the general flow of information around the system. It is a common means of understanding complex data flows around a system within computer science and software engineering…
Flowcharts are very simple diagnostic and modeling structures that follow the logic of the program through a series of linear processes with decision gates, where a yes or no answer is expected, to guide the software to a certain resolution or output.”  (Berry 2011, pp. 113-114)

"the flowchart serves as the central design document around which systems analysts, computer programmers, and end users communicate, negotiate, and represent complexity" (Ensmenger 2016,p. 321)

Conventionally: A flowchart is a diagram that shows the **breakdown of a task** or system into all of the necessary steps. Each step is represented by a symbol and connecting lines show **the step-by-step progression** through the task.

![image4](https://imgs.xkcd.com/comics/flow_charts.png)
![image5](https://wcs.smartdraw.com/flowchart/img/basic-symbols.jpg?bn=1510011109)

**Project Showcase:**

1. **Vocable Code** by Winnie Soon
<img src="flowchart1.png" width=450>

2. **Unerasable Images: Sorting the Physical Slides With an Algorithm**

<img src="flowchart2.jpg" width=450>

3. **[Google will eat itself](http://www.gwei.org/index.php) (2005) by UBERMORGEN, Alessandro Ludovico and Paolo Cirio**

"One of Google's main revenue generators is the "Adsense" program: It places hundreds of thousands of little Google text-ads on websites around the world.

Now we have set up a vast amount of such Adsense-Accounts for our hidden Web-Sites. Each time someone visits a Web-Sites within our network of sites, he/she triggers a series of robots. For each click we receive a micropayment from Google.

Google pays us monthly by check. Each time we collect enough money, we buy the next Google share.

GWEI - Google Will Eat Itself is to show-case and unveil a total **monopoly of information**, a weakness of the new **global advertisment system** and the renaissance of the “new economic bubble" - reality is, Google is currently valued more than all Swiss Banks together (sic)."

![image3](http://www.gwei.org/img/GWEI_Johannesburg1.jpg)
![image1](http://www.gwei.org/img/diag_gwei_attack.gif)

More different flowcharts [here](http://www.gwei.org/pages/diagram/diagram.html)

4. **The Project Formerly Known as Kindle Forkbomb Printing Press by UBERMORGEN**

<img src="forkbomb.png" width=450>

<img src="http://kunsthal.dk/sites/default/files/styles/medium/public/page-images/sys2_044_0.jpg" width=450>

<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTagSaTPrVkEkE4aF657TJrm1ltKIrbLvAjgVfVyOr8GCMakPUa">

Background:
- digital books on Amazon
- the Amazon  Kindle  e-book  reader,  and  the  Amazon  Fire  tablet  as  well  print-on-demand  and  publishing  services  (Amazon  Publishing  and   Amazon  Create Space) — with direct sales through to the Amazon Book Store
- Evalation and comment system on books. => directing what to buy or "most likely to buy" for profit-making

The work:
- "inspired  by  the  negative  comments  on  Rebecca Black's Friday YouTube video."
- "In 2012, they collaborated with Luc Gross and Bernhard  Bauch  to  build  an  Internet  robot  that  could  automatically  generate books on the basis of YouTube comments on videos and upload them as e-books to Amazon’s Kindle bookstore — producing a whole literary ecology including crowds,  authors,  books,  titles,  accounts,  pricing  and  a  defence  system  against erasure"
- "the project aims to work as a ‘forkbomb’in  the  system,  by  automatizing  the  networked  processes  that  produce  language today, and turning language against itself. In computing, a forkbomb is a denial-of-service  attack,  meaning  it  is  a  process  that  continually  replicates  itself, draining the system of its resources, ultimately causing the system to crash.""

See ref: (Pold and Andersen 2013) http://www.ieeff.org/f18pold.pdf

**Discussion**
1. Can you give a daily example about an algorithm that you have used or experienced. Please discuss the **(computational) logic** in details.

2. Base on the assigned reading from Bucher, can you list out some of the properties of algorithm? Why is it both technical and social?

##### 2. Peer-tutoring: Group 8 / Respondents: Group 9, Topic: A sorting algorithm

**Task:**
Generate a list of 1000 unique and random integer between two number ranges. Then implement a sorting algorithm and display them in a chronological order (You can't use the sort function in p5 or javascript). How wold you approach this problem? What kind of programming practice are you using? How would you visualize it via a flowchart? How is sorting been used in digital culture? What is sorting in both technical and conceptual sense?

**Discussion in groups:**
Generate a list of 1000 unique and random integer between two number ranges. Then implement a sorting algorithm and display them in a chronological order (You can't use the sort function in p5 or javascript).

https://editor.p5js.org/siusoon/sketches/7g1F594D5

##### 3. in-class ex: draw a flow chart based on below code
```javascript
function setup() {
  var sometext = ['h','e','l','l','o'];
  for (var i=0;i<sometext.length; i++) {
    console.log(sometext[i]);
  }
}

/*output
h
e
l
l
o
*/
```
Tools:
1. https://www.draw.io/
2. https://creately.com/
...

[here](https://www.draw.io/?lightbox=1&highlight=0000ff&edit=_blank&layers=1&nav=1&title=flowchart%20-%20for%20loop#R7Vhdb5swFP01SNvLxHeSxzZLuknbVKkPax9dcMCr8UXGach%2B%2FWwwBeOmrTqattOIguzj6%2Bvrw7k2xgmWRX3GUZl%2FhxRTx3fT2gk%2BO74fLgJ5V8C%2BBRazeQtknKQt5PXABfmNNehqdEtSXBmGAoAKUppgAozhRBgY4hx2ptkGqDlqiTJsARcJojb6k6Qib9F55Pb4F0yyvBvZc3XLNUpuMg5bpsdz%2FGDTXG1zgTpf2r7KUQq7waDBygmWHEC0paJeYqqo7Whr%2B60PtN7FzTETT%2BnQ9ajEvps7TiUVugpc5JABQ3TVo6fN%2FLDy4MpaLgoqi54sykH5%2FlLhn6KueqXNfmEh9vpJo60ACfXevwGU2oc9gy5E2PJEx%2BhrTSCeYW0VtZCKfkynVCmGAstgpAHHFAlyaz5opPWS3dn1nMmCpu0Aha2LW0S32mkl4xI2r5RK%2BSr%2BdjkR%2BKJEzWx2MoFMFlFVtprekFqxfLohlC6BAm8cBZtI%2FSReCQ43eNASN9dDJN5iLnD9IEG6NZhrie7N1Nz1%2BRBqKB%2BkQof9DaNeaFE6pUonVWJkKzF%2BLSVGFm1fGREEUTVX6Vr%2BKamkMl3YqMUzRxwlAvPKYtfk7hG9TiG20BTb3Bab59%2BjtngCtS3ej9hiW2yzV1v2JqUJ10RcaliVr8xN5HK4wfRtL01uJ7njsxtbomymI8PLJYdZroJCST7MYic4kdU1YaSSKbO2czqH4npbHSefg9HmcV9C37d9eJPsH57F3ptN6ZmtuvlriW5m0XbOiYzdX1PIGu1hQ3C%2B%2BwFYjwOrgKpSlXCM2cc3t6sE8RG3FW%2F%2BfkQ4ueJ013No5NM9j9AdrQqzEc9tFuhePdUnnKP9wKxUBtXhcaLIHCdcjA5Bj9h78cP21jxMe1loI%2B51csfhk6RjK%2BcMVJqpG8O1MJPwjSVZeNQks195f4BFiVCcGfM2T04MGB4dszQkX50zJquJ5EftsKeKF5IgeqIbCpKm9NCZznwUU3C9CM0FrXt5feRQ5k9AdedjQPWKpfaS9r5Ouf4o9f3AVu9LHXP94L94jyVe%2B4vCFbaPvf8M1%2F7iebJ%2BBtey2n%2BubDe7%2FpNwsPoD)

A flow chart shows a breakdown of tasks representing procedures, step-by-step progression and algorithms. It is not a detailed flow chart that includes every possible computational steps but rather it demonstrates high-level processes for **visual understanding** that gives a general idea of how tasks are broken down. It is independent of any programming language as it **concerns procedures** but not coding syntax.

**Discussion**

According to Ensmenger (2016):

1.  how would you understand "To view the computer flowchart as having only one purpose is narrow and misleading" as he suggests that "every flowchart had multiple meanings and served several purposes simultaneously"?

2. How would you expand this line of thinking? "Flowcharts allow us to "see" software in ways that are otherwise impossible"

#### 4. Walkthrough next mini-ex10: Flowchart and next
- See [here](https://gitlab.com/siusoon/aesthetic-programming/tree/master/Ap2019/all_mini_ex) for the mini ex10: Individual and Group work: Flow Charts
- Peer-tutoring (after Easter): Group 9 / Respondents: Group 1, Topic: node.js
  - What is node.js?
  - Experiment 1 or 2 npm (package manager) and walkthrough with the class?
- Next big check point: **28-Apr-2019** (SUN) Upload the draft final project + flowchart
- Discussion for [final project](https://gitlab.com/siusoon/aesthetic-programming/blob/master/Ap2019/all_mini_ex/finalProject.md)
