#### Class 05 | Week 10 | 5 Mar 2019: Pause
##### NO Wed tutorial session and NO Fri shutup and code session

### Messy notes:

#### Agenda:
1. Introduction
2. Guest lecture by Dominique Cunin on "software writing for and by the arts : some statements"
3. Peer-tutoring (group 3 and 4)
4. Discussion on (Aesthetic) Programming
5. Walkthrough next mini-ex5: Revisit the past
---

#### 4. Discussion on (Aesthetic) Programming

**Task 1:**
(15 mins) Discuss the question within your own group. Write at least 3 findings and reflections on the [discussion board](https://padlet.com/wsoon/aestheticprogramming).

**Task 2:**
(10 mins) Share your (summarized) ideas across groups:

- Group 1-2-3
- Group 4-5
- Group 6-7
- Group 8-9

**Task 3:**
After listening to different groups' idea, each group will try to summarize and synthesize the discussion and present them to the class.

** Questions -

Group 1:
> The analogy of Coding and Writing has been used by scholars like Annette Vee and Alan Perlis, especially the concept of mass literacy in which pervasive changes to society is necessary. According to Vee, what is the concept of literacy? (Please stick with Vee's argument and articulation and give supporting/evidence to demonstrate your understanding of the text)
Arguably, one of the benefits of having the knowledge of coding is that it could empower computer users to have the knowledge and ability to create and modify software, but more importantly, is having the sensitivity and knowledge to ask the right questions about the assumption and processes of computational infrastructure that govern many of the interfaces and interactions between human and machines. Ultimately, having the coding knowledge might increase accessibility to start reflecting and understanding the blackbox. To what extent do you agree on this? Pick a concrete example (or you can also link to the different types of programming practice by Bergstrom and Blackwell (2016)) that you have experienced throughout the Aesthetic Programming course and expand on Vee's thought. If you do not entirely agree on this, then please reflect upon the potentiality of coding that might bring.

Group 2:
> Nick Montfort suggests 'exploratory programming', which involves using computation as a way of inquiring about and constructively thinking about important issues. He offers argument that programming can 1) allows us to think in new ways 2. offers us a better understanding of culture and media systems 3. can help us improve society. Please articulate his thoughts by sticking closely with his text, and then based on that and reflect on your own coding practice to see how these may apply in your case.

Group 3:
> "[T]hose who have background in the arts and humanities and who choose to learn programming are diversifying their ways of thinking, adding to the methods and perspectives that they already have. Programming can help them consider the questions they care about in new ways" (Nick Montfort 2017, p. 270) What do you think about his thoughts? How has programming changed your way of thinking?

Group 4:
> "[T]hose who have some understanding of programming will gain a better perspective on cultural systems that use computation-as many cultural systems increasingly do. ...[A]fter learning to program people are better at developing cultural systems as experiment about, interventions into, augmentations of, or alternatives to the ones that already exit" (Nick Montfort 2017, p. 273) How would you understand this quote? What's your experience on that and your vision on programming?

Group 5:
> In the article by Bergstrom and Blackwell (2016), the authors illustrate 6 different types of programming practices: 1) software engineering 2) Bricolage & Tinkering 3) Sketching with code 4) Live coding 5) Hacking 6) Code bending. What are the different practices in programming? What's your way of practice? What does it mean by a 'reflective practitioner' in the context of programming practice?

Group 6:
> From Annette Vee's research, the four main dominant argument for learning to code includes: 1/ individual empowerment 2/ learning new ways to think 3/ citizenship and collective progress 4/ employability and economic concerns (p. 45) To what extent do you agree on this? What are the consequences that we need to be aware when pushing coding as literacy? Please discuss each of the argument in details and think about what coding practice means to you then.

Group 7:
> Nick Montfort has offered several reasons to have the need to program with a focus on art and humanities. What are the reasons and to what extent you agree on his thoughts? (Pls stick with his text) Draw upon the article by Bergstrom and Blackwell (2016), explain and articulate by discussing various types of programming practices that might achieve Montfort's views on why program.

Group 8:
> "It explores coding as a practice of reading, writing and building, as well as thinking with and in the world, and understanding the complex computational procedures that underwrite our experiences and realities in digital culture." (Aesthetic Programming Class) According to the article by Bergstrom and Blackwell (2016), What are the different practices in programming? What's your way of practice that specifically addresses to the course's outline?

Group 9:
> Montfort mentions using computation for exploratory inquiry to raise new questions, and then Bergstrom and Blackwell mention Bricolage and Tinkering, are they both referring to the same thing? If no, what are they referring to?

Extra:
> Vee mentions "literacy is not simply an isolated cognitive skills but instead gains its meaning and power in social interactions" (p. 81), what does it mean?

> "If literacy has been ideologically linked to morality and the health of a society, what social goods are now being attributed to programming under the rubric of literacy? What are the ideologies of the new so-called literacy, and what do they mean for programming or for our larger concept of literacy? These new ideologies signal a massive shift in what literacy and communication have become, whom they benefit, and to what ends they might be put." (Vee 2017, p. 46)

> "Literacy no longer implies just reading for comprehension, but also reading for critical thought as well as writing with complex structures and ideas." (Vee 2017, p. 48) If we continuously to use the analogy of writing and coding as literacy, to what extent do you agree coding need to have critical thoughts? Can you articulate with one of your previous sketches that can demonstrate that?

---
#### 5. Walkthrough next mini-ex5 and next week
- See [here](https://gitlab.com/siusoon/aesthetic-programming/tree/master/Ap2019/all_mini_ex) for the mini ex5
- Peer-tutoring: Group 5 / Respondents: Group 6, Topic: p5.play library
- Reminder: 8.Mar.2019 > [Symposium - ScreenShots: Desire & Automated Image](https://www.facebook.com/events/384412612115375/)
